# Qantas REST CRM Challenge

This is a Spring Boot REST API for Customer management.

As the REST API ultimately will be integrating with a backend CRM plus an Authorization Service
please refer to the Security and Assumptions sections for additional information regarding this.

## Security

Spring Security has been used to provide Basic Authentication to the REST API's
A simple in memory authentication provider has been implemented to authorise the following 2 users.

Standard User:
username:user
password:password
role:USER

Admin User:
username: admin
password: password1!
role: ADMIN

Method level security has been enabled so that only the ADMIN user can retrieve all customer profiles.
However both ADMIN and USER can access other API functions.

Please note that in a production environment this would not be the approach that would be taken.
Instead authentication and authorization would be handled via an external Authorization Service Provider
like PingFederate for example using Auth2.0 and rely on JWT stateless tokens signed with an asymmetrical key i.e. RS256

## Assumptions

An in memory H2 Database has been configured using springboot's sensible defaults to act as the CRM in this
coding challenge.

If you would like to see the table structure and the data as it appears in the h2 database.
Please enter the url http://localhost:8080/h2-console
Login Details are:
Saved Settings : Generic H2(Embedded)
Settings Name : Generic H2(Embedded)
Driver class : org.h2.Driver
JDBC URL: jdbc:h2:mem:testdb
User Name: sa
Password :

On connection there will be 2 tables created CUSTOMER and ADDRESS.


In reality the RESTFull API's developed would be acting as a middle ware, a gateway
to the backend CRM. There are multiple ways of inter service communication, synchronous using a RestTemplate
to POST or PUT data to the CRM or asynchronous using Message Queues like Redis or Kafka.

Although a synchronous approach is the easier of the two, it must be noted that this approach relies on the CRM to be
up and to provide a response in a suitable time frame. If the CRM is down for instance then a response of 500 would not
really be acceptable, although this can be mitigated using Hysterix fallbackMethod, there is still the issue that request
was not able to be completed.

A message queue approach like Kafka and Redis store the messages and provide replay functionality in the event that
the initial request fails, thus guaranteeing eventual consistency.

## API Documentation

SWAGGER2 is used for the API documentation.

to view the api please enter http://localhost:8080/swagger-ui.html

## Response Format

The API is based on hateoas so the response will contain links to other actions that the client can perform.
Details of the responses and model objects can be obtained by referring to SAGGER UI

## Content Negotiation

After careful consideration I have made the decision to support application/json and not application/xml
application/xml is a legacy format and moving forward it should slowly be phased out. It is always possible
to add application/xml by including the relevant jackson library.
i.e.

<dependency>
    <groupId>com.fasterxml.jackson.dataformat</groupId>
    <artifactId>jackson-dataformat-xml</artifactId>
</dependency>

and adding the accepts MediaType.APPLICATION_XML_VALUE to the request mapping in the REST methods

## Versioning

In order for the REST API to be extensible a versioning strategy had to be selected. After careful
consideration I have decided to use parameter based versioning thus the URL's would be of the format
similar to the following.

protocol://host:port/customers/profile/1?version=1

The choice to use parameter based versioning is due to the fact that this API is to be consumed
via WEB and MOBILE channels meaning that the urls can be bookmarked. Had I gone with a HEADER based
versioning approach then the only way these API's could be consumed would be via JavaScript requests,
Java based back end systems for e.g. using a RestTemplate or Browser Plugins like Postman.

## Pre-Requisites

JAVA 8
MAVEN 3.5.0
JAVA_HOME, MAVEN_HOME, PATH System variables configured.

if running from intellij or eclipse Lombok plugin installed

## Running the application

Once this repository has been cloned using the
git clone https://ashkolyar@bitbucket.org/ashkolyar/qantas-crm-rest-challenge.git command.

The application can be run using the mvn spring-boot:run command if running from the command line
or via running the QantasRestCrmApplication main class.


