package qantas.rest.crm.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import qantas.rest.crm.exception.ExceptionResponse;
import qantas.rest.crm.exception.ValidationExceptionResponse;
import qantas.rest.crm.model.CustomerResource;
import qantas.rest.crm.service.CustomerService;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@RestController
@RequestMapping("customers")
@Api(value = "customers", description = "Operations pertaining to Customer management")
public class CustomerController {

    private CustomerService customerService;

    @ApiOperation(value = "/all?version=1", nickname = "allCustomers",
            notes = "Retrieves all customers can only be invoked by ADMIN users")

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerResource.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
            @ApiResponse(code = 415, message = "Invalid Content Type", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Failure", response = ExceptionResponse.class)})

    @Secured("ROLE_ADMIN")
    @GetMapping(value = "/all", params = "version=1", produces = MediaType.APPLICATION_JSON_VALUE)

    public HttpEntity<Resources<CustomerResource>> allCustomers() {

        List<CustomerResource> customers = customerService.findAll();


        Resources<CustomerResource> resources = new Resources<>(customers);

        final String uriString = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
        resources.add(new Link(uriString, "self"));


        return new ResponseEntity<>(resources, HttpStatus.OK);

    }


    @ApiOperation(value = "profile/{customerId}?version=1", nickname = "getCustomer", notes = "Retrieves a customer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerResource.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class),
            @ApiResponse(code = 415, message = "Invalid Content Type", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Failure", response = ExceptionResponse.class)})


    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(value = "profile/{customerId}", params = "version=1", produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Resource<CustomerResource>> getCustomer(@PathVariable Long customerId, @RequestParam(value = "version") String version) {

        CustomerResource customer = customerService.findById(customerId);

        Resource<CustomerResource> resource = new Resource<>(customer);

        Link selfLink = linkTo(methodOn(CustomerController.class).getCustomer(customer.getCustomerId(), version)).withSelfRel();
        Link updateLink = linkTo(methodOn(CustomerController.class).updateCustomer(customer.getCustomerId(), customer, version)).withRel("update");
        Link deleteLink = linkTo(methodOn(CustomerController.class).deleteCustomer(customer.getCustomerId(), version)).withRel("delete");

        resource.add(selfLink);
        resource.add(updateLink);
        resource.add(deleteLink);


        return new ResponseEntity<>(resource, HttpStatus.OK);

    }

    @ApiOperation(value = "profile?version=1", nickname = "createCustomer", notes = "Creates a customer and returns the customer object")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Success", response = CustomerResource.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ValidationExceptionResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
            @ApiResponse(code = 415, message = "Invalid Content Type", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Failure", response = ExceptionResponse.class)})

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping(value = "profile", params = "version=1", produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Resource<CustomerResource>> createCustomer(@Valid @RequestBody CustomerResource customerResource, @RequestParam(value = "version") String version) {

        CustomerResource savedCustomer = customerService.save(customerResource);

        Resource<CustomerResource> resource = new Resource<>(savedCustomer);

        Link selfLink = linkTo(methodOn(CustomerController.class).getCustomer(savedCustomer.getCustomerId(), version)).withSelfRel();

        Link updateLink = linkTo(methodOn(CustomerController.class).updateCustomer(savedCustomer.getCustomerId(), savedCustomer, version)).withRel("update");

        Link deleteLink = linkTo(methodOn(CustomerController.class).deleteCustomer(savedCustomer.getCustomerId(), version)).withRel("delete");

        resource.add(selfLink);
        resource.add(updateLink);
        resource.add(deleteLink);


        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @ApiOperation(value = "profile/{customerId}?version=1", nickname = "updateCustomer", notes = "Updates a customer and returns the updated customer object")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerResource.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ValidationExceptionResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class),
            @ApiResponse(code = 415, message = "Invalid Content Type", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Failure", response = ExceptionResponse.class)})

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PutMapping(value = "profile/{customerId}", params = "version=1", produces = MediaType.APPLICATION_JSON_VALUE)

    public HttpEntity<Resource<CustomerResource>> updateCustomer(@PathVariable long customerId, @Valid @RequestBody CustomerResource customerResource, @RequestParam(value = "version") String version) {

        CustomerResource savedCustomer = customerService.update(customerId, customerResource);

        Resource<CustomerResource> resource = new Resource<>(savedCustomer);

        Link selfLink = linkTo(methodOn(CustomerController.class).getCustomer(savedCustomer.getCustomerId(), version)).withSelfRel();
        Link updateLink = linkTo(methodOn(CustomerController.class).updateCustomer(savedCustomer.getCustomerId(), savedCustomer, version)).withRel("update");
        Link deleteLink = linkTo(methodOn(CustomerController.class).deleteCustomer(savedCustomer.getCustomerId(), version)).withRel("delete");

        resource.add(selfLink);
        resource.add(updateLink);
        resource.add(deleteLink);


        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @ApiOperation(value = "profile/{customerId}?version=1", nickname = "deleteCustomer", notes = "Deletes a customer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = CustomerResource.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class),
            @ApiResponse(code = 415, message = "Invalid Content Type", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Failure", response = ExceptionResponse.class)})

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @DeleteMapping(value = "profile/{customerId}", params = "version=1", produces = MediaType.APPLICATION_JSON_VALUE)

    public HttpEntity<Resource<CustomerController>> deleteCustomer(@PathVariable long customerId, @RequestParam(value = "version") String version) {

        customerService.delete(customerId);

        Resource<CustomerController> resource = new Resource<>(this);

        Link selfLink = linkTo(CustomerController.class).withSelfRel();

        resource.add(selfLink);

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }


    @Autowired
    @Qualifier("customerService")
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }


}
