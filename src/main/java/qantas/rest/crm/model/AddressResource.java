package qantas.rest.crm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel

@JsonIgnoreProperties(ignoreUnknown = true)

public class AddressResource {

    @ApiModelProperty(dataType = "long", notes = "Database generated id")
    private Long addressId;

    @ApiModelProperty(dataType = "string", required = true, allowableValues = "RESIDENTIAL,POSTAL,BUSINESS,BILLING,SHIPPING,EMAIL", value = "addressType")
    @NotNull(message = "AddressType must be one of the following RESIDENTIAL,POSTAL,BUSINESS,BILLING,SHIPPING,EMAIL values")
    private String addressType;

    @ApiModelProperty(required = true, notes = "The first address line NOTE: if AddressType is EMAIL then this line is the email address, other fields can be null")
    @NotNull
    private String address1;

    private String address2;
    private String address3;
    private String city;
    private String state;
    private String country;
    private String postCode;


    @Override
    public String toString() {
        return "AddressResource{" +
                "addressId=" + addressId +
                ", addressType='" + addressType + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", address3='" + address3 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", postCode='" + postCode + '\'' +
                '}';
    }
}
