package qantas.rest.crm.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@JsonIgnoreProperties(ignoreUnknown = true)

@ApiModel
public class CustomerResource  {

    @ApiModelProperty(dataType = "long", notes = "Database generated id")
    private long customerId;

    @ApiModelProperty(dataType = "string", required = true, notes="first name of customer min length:2 characters, max length:30 characters", example = "Alex")
    @NotNull(message = "first name must not be null")
    @Size(min=2, max=30)
    private String firstName;

    @ApiModelProperty(dataType = "string", required = true, notes="last name of customer min length:2 characters, max length:30 characters", example = "Shkolyar")
    @NotNull(message = "last name must not be null")
    @Size(min=2, max=30)
    private String lastName;

    @Past(message = "date of birth must be in the past")
    @NotNull(message = "date of birth must not be null")
    @ApiModelProperty(dataType = "Date", required = true, notes="Date of birth must be in the past", example = "1972-06-15")
    private Date dateOfBirth;

    @Builder.Default
    private List<AddressResource> addresses = new ArrayList<>();


    @Override
    public String toString() {
        return "CustomerResource{" +
                "customerId=" + customerId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", addresses=" + addresses +
                '}';
    }
}
