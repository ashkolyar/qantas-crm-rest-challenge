package qantas.rest.crm.repository;

import qantas.rest.crm.repository.Address;

/**
 * Address Types used as a Discriminator in {@link Address}
 * <li>{@link #RESIDENTIAL}</li>
 * <li>{@link #POSTAL}</li>
 * <li>{@link #BUSINESS}</li>
 * <li>{@link #BILLING}</li>
 * <li>{@link #SHIPPING}</li>
 * <li>{@link #EMAIL}</li>
 */

public enum AddressType {
    RESIDENTIAL,POSTAL,BUSINESS,BILLING,SHIPPING,EMAIL
}
