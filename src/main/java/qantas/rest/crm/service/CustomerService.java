package qantas.rest.crm.service;

import qantas.rest.crm.model.CustomerResource;

import java.util.List;


public interface CustomerService {
    List<CustomerResource> findAll();
    CustomerResource findById(long id);
    CustomerResource save(CustomerResource customerResource);
    CustomerResource update(long id, CustomerResource customerResource);
    void delete(long id);
}
