package qantas.rest.crm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import qantas.rest.crm.model.AddressResource;
import qantas.rest.crm.model.CustomerResource;
import qantas.rest.crm.repository.Address;
import qantas.rest.crm.repository.Customer;
import qantas.rest.crm.exception.CustomerNotFoundException;
import qantas.rest.crm.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Qualifier("customerService")
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    public List<CustomerResource> findAll(){
        Iterable<Customer> customers = customerRepository.findAll();

        List<CustomerResource> all = new ArrayList<>();

        customers.forEach(c -> {
            List<AddressResource> savedAddress = new ArrayList<>();

            transformAddress(c, savedAddress);

            all.add(CustomerResource.builder()
                    .customerId(c.getId())
                    .firstName(c.getLastName())
                    .lastName(c.getFirstName())
                    .addresses(savedAddress)
                    .build());
        });

        return all;
    }


    public CustomerResource findById(long id) {
        Optional<Customer> customer = customerRepository.findById(id);

        if (!customer.isPresent()) {
            throw new CustomerNotFoundException("Customer with ID : " + id + " Not Found");
        }

        List<AddressResource> savedAddress = new ArrayList<>();

        transformAddress(customer.get(), savedAddress);
        return CustomerResource.builder()
                .customerId(customer.get().getId())
                .firstName(customer.get().getFirstName())
                .lastName(customer.get().getLastName())
                .dateOfBirth(customer.get().getDateOfBirth())
                .addresses(savedAddress)
                .build();

    }

    public CustomerResource save(CustomerResource customerResource) {
        Customer customer = Customer.builder().firstName(customerResource.getFirstName())
                .lastName(customerResource.getLastName())
                .dateOfBirth(customerResource.getDateOfBirth()).build();


        customerResource.getAddresses().forEach(address -> {
            customer.addAddress(Address.builder()
                    .address1(address.getAddress1())
                    .address2(address.getAddress2())
                    .address3(address.getAddress3())
                    .addressType(address.getAddressType())
                    .city(address.getCity())
                    .state(address.getState())
                    .postCode(address.getPostCode())
                    .build());
        });

        Customer savedCustomer = customerRepository.save(customer);

        List<AddressResource> savedAddress = new ArrayList<>();
        transformAddress(savedCustomer, savedAddress);

        return CustomerResource.builder()
                .customerId(savedCustomer.getId())
                .firstName(savedCustomer.getFirstName())
                .lastName(savedCustomer.getLastName())
                .dateOfBirth(customer.getDateOfBirth())
                .addresses(savedAddress).build();

    }

    public CustomerResource update(long id, CustomerResource customerResource) {

        Optional<Customer> customer = customerRepository.findById(id);

        if (!customer.isPresent()) {
            throw new CustomerNotFoundException("Customer with ID : " + id + " Not Found");
        }

        Customer customerToSave = customer.get();
        customerToSave.setDateOfBirth(customerResource.getDateOfBirth());
        customerToSave.setLastName(customerResource.getLastName());
        customerToSave.setFirstName(customerResource.getFirstName());

        //delete addresses from customer that have been removed from dto
        deleteAddressesFromCustomer(customerResource, customerToSave);

        //update existing addresses
        updateExistingAddresses(customerResource, customerToSave);

        //add any new addressess
        addNewAddressesToCustomer(customerResource, customerToSave);


        Customer saved = customerRepository.save(customerToSave);

        List<AddressResource> savedAddress = new ArrayList<>();
        transformAddress(saved, savedAddress);

        return CustomerResource.builder()
                .customerId(saved.getId())
                .firstName(saved.getFirstName())
                .lastName(saved.getLastName())
                .dateOfBirth(saved.getDateOfBirth())
                .addresses(savedAddress).build();

    }

    public void delete(long id){
        Optional<Customer> customer = customerRepository.findById(id);

        if (!customer.isPresent()) {
            throw new CustomerNotFoundException("Customer with ID : " + id + " Not Found");
        }

        customerRepository.deleteById(id);
    }


    private void deleteAddressesFromCustomer(CustomerResource customerResource, Customer customerToSave) {
        Predicate<Address> addressExists = addr -> {
            for (AddressResource addressResource : customerResource.getAddresses()) {
                if (addr.getId().equals(addressResource.getAddressId())) {
                    return true;
                }
            }

            return false;
        };


        //get the addresses that were deleted i.e. those that appear in the customer entity but not in the dto
        List<Address> addressesToRemove = customerToSave.getAddresses().stream().filter(addressExists.negate()).collect(Collectors.toList());

        //remove the address
        if (!addressesToRemove.isEmpty()) {
            for (Address address : addressesToRemove) {
                customerToSave.removeAddress(address);
            }
        }
    }

    private void updateExistingAddresses(CustomerResource customerResource, Customer customerToSave) {
        for (Address address : customerToSave.getAddresses()) {
            for (AddressResource addressResource : customerResource.getAddresses()) {
                if (addressResource.getAddressId().equals(address.getId())) {
                    address.setAddress1(addressResource.getAddress1());
                    address.setAddress2(addressResource.getAddress2());
                    address.setAddress3(addressResource.getAddress3());
                    address.setAddressType(addressResource.getAddressType());
                    address.setCity(addressResource.getCity());
                    address.setState(addressResource.getState());
                    address.setPostCode(addressResource.getPostCode());

                    break;
                }
            }
        }
    }

    private void addNewAddressesToCustomer(CustomerResource customerResource, Customer customerToSave) {
        //add any new addresses that have been added
        for (AddressResource addressResource : customerResource.getAddresses()) {
            if (addressResource.getAddressId() == null) {
                customerToSave.addAddress(Address.builder()
                        .address1(addressResource.getAddress1())
                        .address2(addressResource.getAddress2())
                        .address3(addressResource.getAddress3())
                        .addressType(addressResource.getAddressType())
                        .city(addressResource.getCity())
                        .state(addressResource.getState())
                        .postCode(addressResource.getPostCode())
                        .build());
            }
        }
    }

    private void transformAddress(Customer customer, List<AddressResource> savedAddress) {
        customer.getAddresses().forEach(address -> {
            savedAddress.add(AddressResource.builder()
                    .addressId(address.getId())
                    .address1(address.getAddress1())
                    .address2(address.getAddress2())
                    .address3(address.getAddress3())
                    .addressType(address.getAddressType())
                    .city(address.getCity())
                    .state(address.getState())
                    .postCode(address.getPostCode())
                    .build());
        });
    }


    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
}
