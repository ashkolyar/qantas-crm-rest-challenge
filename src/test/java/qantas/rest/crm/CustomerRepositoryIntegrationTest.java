package qantas.rest.crm;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import qantas.rest.crm.repository.Address;
import qantas.rest.crm.repository.AddressType;
import qantas.rest.crm.repository.Customer;
import qantas.rest.crm.repository.CustomerRepository;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryIntegrationTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CustomerRepository customerRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void given_ValidCustomerId_When_FindById_then_ReturnCustomer() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        Date date = Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Customer myCustomer = Customer
                .builder()
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(date)
                .build();

        myCustomer.addAddress(Address
                .builder()
                .addressType(AddressType.EMAIL.name())
                .address1("ashkolyar@yahoo.com")
                .build());

        Customer saved = entityManager.persist(myCustomer);
        entityManager.flush();

        Optional<Customer> customer = customerRepository.findById(saved.getId());

        Assert.assertThat(true, is(equalTo(customer.isPresent())));
        Assert.assertThat("Alex", is(equalTo(customer.get().getFirstName())));
        Assert.assertThat("Shkolyar", is(equalTo(customer.get().getLastName())));
        Assert.assertThat(1, is(equalTo(customer.get().getAddresses().size())));
        Assert.assertThat("EMAIL", is(equalTo(customer.get().getAddresses().get(0).getAddressType())));
    }

    @Test
    public void given_ValidCustomer_When_Delete_then_ReturnNoCustomer() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        Date date = Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Customer myCustomer = Customer
                .builder()
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(date)
                .build();

        Customer saved = entityManager.persist(myCustomer);
        entityManager.flush();

        customerRepository.deleteById(saved.getId());

        Optional<Customer> customer = customerRepository.findById(saved.getId());

        Assert.assertThat(false, is(equalTo(customer.isPresent())));
    }

    @Test(expected = ConstraintViolationException.class)
    public void given_InValidCustomer_When_Save_then_ThrowConstraintValidationException() {

        Customer myCustomer = Customer
                .builder()
                .firstName("Alex")
                .build();

        entityManager.persist(myCustomer);
        entityManager.flush();
    }

}
