package qantas.rest.crm;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import qantas.rest.crm.api.CustomerController;
import qantas.rest.crm.model.CustomerResource;
import qantas.rest.crm.service.CustomerService;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
@EnableWebMvc
public class CustomerRestControllerTest {

    private static String BASE_PATH = "http://localhost/customers";

    private JacksonTester<CustomerResource> jsonConverter;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mvc;

    @MockBean
    @Qualifier("customerService")
    private CustomerService customerService;


    public CustomerRestControllerTest() {
    }

    @Before
    public void setup() {
        JacksonTester.initFields(this, objectMapper);

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .alwaysDo(MockMvcResultHandlers.print())
                .apply(springSecurity())
                .build();
    }

    @Test
    public void given_NoAuthentication_When_ApiCalled_Then_Return401() throws Exception {
        mvc.perform(get("/customers/profile/1?version=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void given_AuthenticationWithRoleUserProvided_When_ApiCalled_Then_Return200() throws Exception {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        CustomerResource customer = CustomerResource.builder()
                .customerId(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        given(customerService.findById(1)).willReturn(customer);
        mvc.perform(get("/customers/profile/1?version=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void given_AvalidId_When_ApiCalled_Then_ReturnCustomerResource() throws Exception {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        CustomerResource customer = CustomerResource.builder()
                .customerId(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        given(customerService.findById(1)).willReturn(customer);

        mvc.perform(get("/customers/profile/1?version=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("firstName", is(customer.getFirstName())))
                .andExpect(jsonPath("dateOfBirth", is(customer.getDateOfBirth().toString())));

        verify(customerService, times(1)).findById(1);
        verifyNoMoreInteractions(customerService);

    }

    @Test
    @WithMockUser(roles = "USER")
    public void given_CustomerResource_When_POSTResource_Then_ReturnCustomerResource() throws Exception {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        CustomerResource savedCustomer = CustomerResource.builder()
                .customerId(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        final String json = jsonConverter.write(savedCustomer).getJson();

        when(customerService.save(any())).thenReturn(savedCustomer);


        mvc.perform(post("/customers/profile?version=1")
                .with(csrf())
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())

                .andExpect(jsonPath("firstName", is(savedCustomer.getFirstName())))
                .andExpect(jsonPath("dateOfBirth", is(savedCustomer.getDateOfBirth().toString())))
                .andExpect(jsonPath("_links.self.href", is(BASE_PATH + "/profile/1?version=1")))
                .andExpect(jsonPath("_links.update.href", is(BASE_PATH + "/profile/1?version=1")))
                .andExpect(jsonPath("_links.delete.href", is(BASE_PATH + "/profile/1?version=1")));

        verify(customerService, times(1)).save(any());
        verifyNoMoreInteractions(customerService);
    }


    @Test
    @WithMockUser(roles = "USER")
    public void given_InvalidResource_When_POSTResource_Then_Return400() throws Exception {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        CustomerResource savedCustomer = CustomerResource.builder()
                .customerId(1L)
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        final String json = jsonConverter.write(savedCustomer).getJson();

        mvc.perform(post("/customers/profile?version=1")
                .with(csrf())
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(customerService, times(0)).save(any());
        verifyNoMoreInteractions(customerService);
    }


    /**
     * required to get around the error created by @@EnableJpaRepositories in the {@link QantasRestCrmApplication}
     */
    @Configuration
    @ComponentScan(basePackageClasses = {CustomerController.class})
    public static class TestConfigurationClass {

    }
}
