package qantas.rest.crm;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import qantas.rest.crm.exception.CustomerNotFoundException;
import qantas.rest.crm.model.AddressResource;
import qantas.rest.crm.model.CustomerResource;
import qantas.rest.crm.repository.Address;
import qantas.rest.crm.repository.AddressType;
import qantas.rest.crm.repository.Customer;
import qantas.rest.crm.repository.CustomerRepository;
import qantas.rest.crm.service.CustomerService;
import qantas.rest.crm.service.CustomerServiceImpl;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceIntegrationTest {


    @TestConfiguration
    static class CustomerServiceImplTestContextConfiguration {

        @Bean
        public CustomerService customerService() {
            return new CustomerServiceImpl();
        }
    }

    @Autowired
    @Qualifier("customerService")
    private CustomerService customerService;

    @MockBean
    private CustomerRepository customerRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Before
    public void setUp() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        Customer customer = Customer
                .builder()
                .id(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        Mockito.when(customerRepository.findById(customer.getId())).thenReturn(Optional.of(customer));
    }

    @Test
    public void given_ValidCustomerId_When_FindById_then_ReturnCustomerResource() {
        CustomerResource found = customerService.findById(1);

        Assert.assertThat("Alex",is(equalTo(found.getFirstName())));
        Assert.assertThat("Shkolyar",is(equalTo(found.getLastName())));
    }


    @Test
    public void given_InvalidCustomerId_When_FindByIdInvoked_Then_CustomerNotFoundExceptionShouldBeThrown() {

        thrown.expect(CustomerNotFoundException.class);
        thrown.expectMessage("Customer with ID : 2 Not Found");
        customerService.findById(2);
    }

    @Test
    public void given_InvalidCustomerId_When_DeleteInvoked_Then_CustomerNotFoundExceptionShouldBeThrown() {

        thrown.expect(CustomerNotFoundException.class);
        thrown.expectMessage("Customer with ID : 2 Not Found");
        customerService.delete(2);
    }

    @Test
    public void given_InvalidCustomerId_When_UpdateInvoked_Then_CustomerNotFoundExceptionShouldBeThrown() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);
        CustomerResource customer = CustomerResource
                .builder()
                .customerId(2L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        thrown.expect(CustomerNotFoundException.class);
        thrown.expectMessage("Customer with ID : 2 Not Found");

        customerService.update(2l,customer);
    }

    @Test
    public void given_CustomerResource_When_SaveInvoked_Then_ReturnSavedCustomerResource() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);

        Customer customer = Customer
                .builder()
                .id(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();


        CustomerResource customerResource = CustomerResource
                .builder()
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        Mockito.when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        CustomerResource saved = customerService.save(customerResource);

        Assert.assertNotNull(saved);
        Assert.assertThat(1L,is(equalTo(saved.getCustomerId())));

        verify(customerRepository, times(1)).save(ArgumentMatchers.any());
        verifyNoMoreInteractions(customerRepository);
    }

    @Test
    public void given_CustomerResource_When_UpdateInvoked_Then_ReturnSavedCustomerResourceWithNewAddressAdded() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);

        Customer customer = Customer
                .builder()
                .id(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        customer.addAddress(Address
                .builder()
                .addressType(AddressType.EMAIL.name())
                .address1("ashkolyar@yahoo.com")
                .build());

        CustomerResource customerResource = customerService.findById(1L);

        Assert.assertNotNull(customerResource);

        customerResource
                .getAddresses()
                .add(AddressResource
                        .builder()
                        .addressType(AddressType.EMAIL.name())
                        .address1("ashkolyar@yahoo.com")
                        .build());



        Mockito.when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        CustomerResource updated = customerService.update(1L, customerResource);

        Assert.assertThat(1,is(equalTo(updated.getAddresses().size())));
        Assert.assertThat("EMAIL",is(equalTo(updated.getAddresses().get(0).getAddressType())));

        verify(customerRepository, times(2)).findById(ArgumentMatchers.anyLong());
        verify(customerRepository, times(1)).save(ArgumentMatchers.any());
        verifyNoMoreInteractions(customerRepository);
    }

    @Test
    public void given_CustomerResource_When_UpdateInvoked_Then_ReturnSavedCustomerResourceWithAddressModified() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);

        Customer customer = Customer
                .builder()
                .id(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();

        customer.addAddress(Address
                .builder()
                .addressType(AddressType.RESIDENTIAL.name())
                .address1("36-40 Gladstone Street")
                .city("North Parramatta")
                .state("NSW")
                .country("Australia")
                .postCode("2151")
                .build());


        CustomerResource customerResource = customerService.findById(1L);

        Assert.assertNotNull(customerResource);

        customerResource
                .getAddresses()
                .add(AddressResource
                        .builder()
                        .addressType(AddressType.EMAIL.name())
                        .address1("ashkolyar@yahoo.com")
                        .build());


        Mockito.when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        CustomerResource updated = customerService.update(1L, customerResource);

        Assert.assertThat("RESIDENTIAL",is(equalTo(updated.getAddresses().get(0).getAddressType())));


        verify(customerRepository, times(2)).findById(ArgumentMatchers.anyLong());
        verify(customerRepository, times(1)).save(ArgumentMatchers.any());
        verifyNoMoreInteractions(customerRepository);
    }

    @Test
    public void given_CustomerResource_When_UpdateInvoked_Then_ReturnSavedCustomerResourceWithAddressDeleted() {
        LocalDate birthDate = LocalDate.of(1972, Month.JUNE, 15);

        Customer customer = Customer
                .builder()
                .id(1L)
                .firstName("Alex")
                .lastName("Shkolyar")
                .dateOfBirth(Date.valueOf(birthDate))
                .build();


        CustomerResource customerResource = customerService.findById(1L);

        Assert.assertNotNull(customerResource);


        Mockito.when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        CustomerResource updated = customerService.update(1L, customerResource);

        Assert.assertThat(0,is(equalTo(updated.getAddresses().size())));


        verify(customerRepository, times(2)).findById(ArgumentMatchers.anyLong());
        verify(customerRepository, times(1)).save(ArgumentMatchers.any());
        verifyNoMoreInteractions(customerRepository);
    }
}
