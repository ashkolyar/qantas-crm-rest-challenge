package qantas.rest.crm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableAutoConfiguration
@EntityScan(basePackages = { "qantas.rest.crm" })
public class QantasRestCrmApplicationTests {

	@Test
	public void contextLoads() {
	}

}
